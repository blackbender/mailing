<?php

namespace Database\Seeders;


use App\Models\Event;
use App\Models\Subscriber;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $usersCount = 100;
        $eventsPerUser = 20;
        $subscribersPerEvent = 1000;
        $subsCount = 10000;
        $subscribers = Subscriber::factory($subsCount)->create();

        $subsRandIdArr = array_rand($subscribers->toArray(), $subscribersPerEvent);

        \App\Models\User::factory($usersCount)->create()->each(
            function ($user) use ($subsRandIdArr, $eventsPerUser) {
                Event::factory($eventsPerUser)->create(['user_id' => $user->id])->each(
                    function ($event) use ($subsRandIdArr) {
                        $event->subscribers()->attach($subsRandIdArr);
                });
        });
    }
}
