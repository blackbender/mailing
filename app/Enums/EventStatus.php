<?php

namespace App\Enums;

use BenSampo\Enum\Enum;


final class EventStatus extends Enum
{
    const Created = 0;
    const Ready = 1;
    const Started = 2;
    const Ended = 3;

//    public static function getNameByValue(EventStatus $enumValue)
//    {
//        switch ($enumValue){
//            case 0:
//                return 'Created';
//            case 1:
//                return 'Ready';
//            case 2:
//                return 'Started';
//            case 3:
//                return 'Ended';
//        }
//    }
}
