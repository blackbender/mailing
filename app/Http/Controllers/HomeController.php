<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index(Request $request)
    {

        $events = Event::with(['user'])->paginate(20);

        foreach ($events as $event) {
            $event->author = Auth::id() == $event->user_id;
        }

        return view('home', ['events' => $events]);
    }
}
