<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class SubsController extends Controller
{
    public function showAttachForm($id, Request $request)
    {
        $event = Event::find($id);
//        $prevUrl = URL::previous();
        Cookie::queue('redirect', URL::previous());
        return view('attaching.form', ['event' => $event]);
    }

    public function attachToEvent($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'email'],
            'first_name' => ['required'],
            'last_name' => ['required']
        ]);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $subscriber = Subscriber::firstOrNew([
            'email' => $request->email
        ]);

        if (!isset($subscriber->first_name)){
            $subscriber->first_name = $request->first_name;
            $subscriber->last_name = $request->last_name;
            $subscriber->save();
        }

        $event = Event::find($id);
        if(!$subscriber->events()->get()->contains($event)){
            $subscriber->events()->attach($id);
        }

        return redirect()->to(Cookie::get('redirect'));


    }
}
