<?php

namespace App\Http\Controllers;

use App\Enums\EventStatus;
use App\Models\Event;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function profile(Request $request)
    {
        $events = Event::with(['subscribers'])->where('user_id', '=', $request->user()->id)
            ->paginate(20);

        foreach ($events as $event) {
            $event->status = EventStatus::fromValue($event->status)->key;
        }
        return view('user.page', ['user' => $request->user(), 'events' => $events]);
    }
}
