<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessEvents;
use App\Models\Event;
use App\Enums\EventStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    public function view($id, Request $request)
    {
        $event = Event::with(['user', 'subscribers'])->find($id);
        $event->status = EventStatus::fromValue($event->status);
        return view('event.view', ['event' => $event]);
    }

    public function createForm(Request $request)
    {
        return view('event.form');
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required'],
            'description' => ['required']
        ]);
        if ($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $event = new Event([
            'user_id' => $request->user()->id,
            'title' => $request->title,
            'description' => $request->description
        ]);

        $event->save();

        return redirect()->route('viewEvent', ['id' => $event->id]);

    }

    public function updateForm($id, Request $request)
    {
        $event = Event::find($id);
        $eventStatuses = EventStatus::asSelectArray();
        return view('event.form', ['event' => $event, 'statuses' => $eventStatuses]);
    }

    public function update($id, Request $request)
    {

        $validator = Validator::make($request->all(), [
            'title' => ['required'],
            'description' => ['required'],
            'status' => ['numeric', function($attribute, $value, $fail){
                if(!EventStatus::hasValue((int)$value))
                {
                    $fail('Given status isn`t exist');
                }}]
        ]);
        if ($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $event = Event::find($id);

        if ($request->status == EventStatus::Started && $event->status != EventStatus::Ended){
            ProcessEvents::dispatch($event);
        }
        else{
            $event->status = $request->status;

        }

        $event->title = $request->title;
        $event->description = $request->description;
        $event->save();

        return redirect()->back();
    }

    public function delete($id, Request $request)
    {

        $event = Event::find($id);
        $event->delete();

        return redirect()->back();

    }
}
