<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class ProcessMailing implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $subscribers;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($subscribers)
    {
        //
        $this->subscribers = $subscribers;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->subscribers as $subscriber) {
            Mail::send('emails.subscriber', ['subscriber' => $subscriber], function ($message) use ($subscriber) {
                $message->from('test@example.net', 'Mailing');
                $message->to($subscriber->email, $subscriber->first_name.' '.$subscriber->last_name)
                    ->subject('Mailing Notification');
            });

        }
    }
}
