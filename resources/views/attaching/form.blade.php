@extends('layouts.mainLayout')

@section('content')
    <div class="row justify-content-center mb-3">
        <h2 class="col-8">Subscribe on event: <b>{{$event->title}}</b> </h2>
    </div>
    <div class="row justify-content-center">
        <form method="POST" action="{{route('attachToEvent', ['id' => $event->id])}}" class="col-6">
            @csrf
{{--            <input type="hidden" name="prevUrl" value="{{$prevUrl}}">--}}
            <div class="form-floating mb-3">
                <input type="email" class="form-control" id="floatingInput" name="email" placeholder="name@example.com">
                <label for="floatingInput">Email address</label>
            </div>
            <div class="form-floating mb-3">
                <input type="text" class="form-control" id="floatingFName" name="first_name" placeholder="First Name">
                <label for="floatingFName">First Name</label>
            </div>
            <div class="form-floating mb-3">
                <input type="text" class="form-control" id="floatingLName" name="last_name" placeholder="Last Name">
                <label for="floatingLName">Last Name</label>
            </div>
            <div>
                <button type="submit" class="btn btn-danger">Subscribe</button>
            </div>
        </form>
    </div>
@endsection
