@extends('layouts.mainLayout')

@section('content')
    <form method="POST" action="{{(isset($event)) ? route('updateEvent', ['id' => $event->id]):route('createEvent')}}">
        @csrf

        <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Event title</label>
            <input type="text" class="form-control" name="title" value="{{$event->title ?? ''}}"
                   id="exampleFormControlInput1" placeholder="Title">
        </div>
        <div class="mb-3">
            <label for="exampleFormControlTextarea1" class="form-label">Event description</label>
            <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="5"
                      placeholder="Description">{{$event->description ?? ''}}</textarea>
        </div>
        @if(isset($event))
            <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label" id="statusSelect">Choose event status</label>
                <select class="form-select form-select-lg" name="status" id="statusSelect" aria-label="form-select-lg example">
                    @foreach($statuses as $status => $value)
                        <option value="{{$status}}" {{($event->status == $status) ? 'selected' : ''}}>{{$value}}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        @else
            <button type="submit" class="btn btn-primary">Create</button>
        @endif


    </form>
@endsection
