@extends('layouts.mainLayout')

@section('content')
    <div class="row justify-content-between">
        <div class="col">
            <h3>{{$event->title}}</h3>
            <p>{{$event->description}}</p>
            <p>{{$event->user->name}}</p>
            <p>Status: {{$event->status->key}}</p>
            <hr>
        </div>
    </div>
@endsection
