@extends('layouts.mainLayout')

@section('content')
    <h2>User: {{$user->name}}</h2>
    <h3>Events:</h3>
    <div class="w-100"></div>
    <div class="row">
        @forelse($events as $event)
            <div>
                <a href="{{route('viewEvent', ['id'=>$event->id])}}">
                    <h4>{{$event->title}}</h4>
                    <p>{{$event->description}}</p>
                    <p>Status: <strong>{{$event->status}}</strong></p>
                    <p>Subs count: {{$event->subscribers->count()}}</p>
                </a>
                <hr>
            </div>
        @empty
            <div class="col">
                <p>No ads</p>
            </div>
        @endforelse
        {{$events->links()}}
    </div>
@endsection
