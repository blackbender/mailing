@extends('layouts.mainLayout')

@section('content')

        <div class="row justify-content-between">
            @forelse($events as $event)
                <div class="col-4">
                    <a href="{{route('viewEvent', ['id'=>$event->id])}}">
                        <h3>{{$event->title}}</h3>
                        <p>{{$event->description}}</p>
                        <p>{{$event->user->name}}</p>
                    </a>
                    @if($event->author)
                    <div>
                        <a href="{{route('updateEventForm', ['id' => $event->id])}}" class="btn btn-success">Update</a>
                        <a href="{{route('deleteEvent', ['id' => $event->id])}}" class="btn btn-secondary">Delete</a>
                    </div>
                    <hr>
                    @endif
                    <a href="{{route('showAttachForm', ['id' => $event->id])}}" type="button"
                        class="btn btn-danger">Subscribe</a>
                    <hr>
                </div>
            @empty
                <div class="col">
                    <p>No ads</p>
                </div>
            @endforelse
        {{$events->links()}}
        </div>
@endsection
