<?php

use App\Http\Controllers\EventController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\SubsController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])
    ->name('home');

Route::get('/event/view/{id}', [EventController::class, 'view'])
    ->name('viewEvent');
Route::get('/login', [LoginController::class, 'index'])
    ->name('loginForm')
    ->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate'])
    ->name('authenticate');
Route::get('/logout', [LoginController::class, 'logout'])
    ->name('logout')
    ->middleware('auth');

Route::get('/user/profile', [UserController::class, 'profile'])
    ->name('userProfile')
    ->middleware('auth');

Route::prefix('attach')->group(function(){
    Route::get('/{id}', [SubsController::class, 'showAttachForm'])
        ->name('showAttachForm');
    Route::post('/{id}', [SubsController::class, 'attachToEvent'])
        ->name('attachToEvent');
});

Route::prefix('event')->middleware(['auth'])->group(function (){
    Route::get('create', [EventController::class, 'createForm'])
        ->name('createEventForm');
    Route::post('create', [EventController::class, 'create'])
        ->name('createEvent');

//    Route::middleware('')
    Route::get('update/{id}', [EventController::class, 'updateForm'])
        ->name('updateEventForm');
    Route::post('update/{id}', [EventController::class, 'update'])
        ->name('updateEvent');

    Route::get('/delete/{id}', [EventController::class, 'delete'])
        ->name('deleteEvent');
});



